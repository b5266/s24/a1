db.users.find({
	$or: [
	{firstName: { $regex: 's', $options: '$i' } },
	{lastName: { $regex: 'd', $options: '$i' } }
	]
}, { firstName: 1, lastName: 1, _id: 0 }).pretty();

// 2. Find users who are from the HR department and their age is greater than or equal to 70.
db.users.find({
	$and: [
		{
			age: {
					$gte: 70
			}
		},
		{
			department: "HR"
		}
	]
})
// Find a users with the letter 'e' in their first name and has an age of less than or equal to 30
db.users.find({
	$and: [
		{
			age: {
					$lte: 30
			}
		},
		{
			firstName: {$regex: 'e', $options: '$i'}
		}
	]
})